module.exports = function(grunt) {

// Project configuration.
  grunt.initConfig({

    jshint: {
    options: {
      reporter: require('jshint-stylish'),
    },
    target: ['HTML/**/main.js']
  },
  sass: {                           
      dist: {                             
        options: {                       
            style: 'expanded',
            sourcemap: 'none',
        },
        files: {                         
            'HTML/assets/css/style.css': 'HTML/assets/sass/style.scss',  // 'destination': 'source' 
        }
      }
  },
  watch: {
      js: {
        files: ['HTML/**/main.js'],
        tasks: ['jshint'],
        options: {
        },
      },
      sass: {
        files: ['HTML/**/sass/*.scss'],
        tasks: ['sass'],
        options: {
        },
      },
  },

  });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);

};