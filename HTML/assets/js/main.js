var $devicewidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
var $deviceheight = (window.innerHeight > 0) ? window.innerHeight : screen.height;

(function($) {



    $(document).ready(function(){

        /*Video by click*/

        var video = $(".ct-js-videoClick"),
            body = $("body");

        $(".video-container").hide();
        video.on("click",function(e){
            body.find(".video-container").fadeIn("fast",function() {
                body.css("overflow-y","hidden");
            }).append("<div class='button-close'></div>");
            $('.button-close').click(function(e){
                body.find('.video-container').fadeOut();
                body.css("overflow-y","auto");
                $(".video-container").find(iframe).remove();
            });
            var url = 'http://www.youtube.com/embed/fC3Cthm0HFU?feature=player_detailpage&amp;autoplay=1';
            var iframe = $("<iframe src='' class='ct-iframe'></iframe>").attr("src",url)
            $(".video-container").prepend(iframe);
            e.preventDefault();
        });

    });
})(jQuery);